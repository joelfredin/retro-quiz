#include "newcircle.h"

NewCircle::NewCircle()
{

}

NewCircle::~NewCircle()
{

}

QRectF NewCircle::boundingRect() const
{
    return QRectF(20, 100, 50, 50);
}

void NewCircle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRectF rect = boundingRect();
       QPen pen(Qt::NoPen);
       QBrush brush(Qt::blue);
       brush.setStyle(Qt::SolidPattern);

       painter->setPen(pen);
       painter->setBrush(brush);
       painter->drawEllipse(rect);
}
