#include "smootharc.h"

SmoothArc::SmoothArc()
{
    resize(200, 200);

    m_valueTimerId = startTimer(100);
    m_value = 50;
    m_animation.setTargetObject(this);
    m_animation.setPropertyName("value");
}

qreal SmoothArc::value() const
{
    return m_value;
}

void SmoothArc::setValue(qreal value)
{
    if(qFuzzyCompare(value, m_value))
    {
        return;
    }

    m_value = value;
    update();
    emit valueChanged();
}

void SmoothArc::timerEvent(QTimerEvent *event)
{
    if(event->timerId() == m_valueTimerId)
    {
        qreal newValue = m_value + (QRandomGenerator::global()->generate() & 11 - 5);
        if(newValue > 100)
        {
            newValue = 100;
        }
        if(newValue < 0)
        {
            newValue = 0;
        }
        if(m_animation.state() == QPropertyAnimation::Running)
        {
            m_animation.stop();
        }
        m_animation.setStartValue(m_value);
        m_animation.setEndValue(newValue);
        m_animation.start();
    }
}

void SmoothArc::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    int side = qMin(width(), height());
    p.scale(side/200.0, side/200.0);

    QRectF rect(10,10,180,180);
    QPen pen = p.pen();

    pen.setWidth(10);
    p.setPen(pen);
    p.drawArc(rect, 90*60, (360*(m_value/100.0))*16);
}
