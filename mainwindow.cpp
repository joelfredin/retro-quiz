#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <stdlib.h>
#include <time.h>
#include <QPlainTextEdit>
#include <QVector>
#include <QRandomGenerator>
#include <QPropertyAnimation>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include<QtQuick>
#include <QOpenGLWidget>
#include <QQuickWidget>
#include <QQuickWindow>
#include <QPainter>
#include <QTime>
#include <QRegion>
#include <QTimer>
#include <QPropertyAnimation>
#include <iostream>     // std::cout
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock
#include "questionform.h"
#include "smootharc.h"
#include "widget.h"
#include <QRect>

// Defining vector of all users, one for all questions with answers, and one where I store all points.
QVector<QString> users{};

QVector<std::pair<QString, int>> high_score{};

int points;

QuestionForm *questionform = new QuestionForm;


// Setups the Mainwindow, styles the push-buttons and assign questions and answers to the different windows.

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QPixmap bkgnd("C:/Users/Joel/Pictures/the_real_retro.png");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);


    ui->lineEdit->setStyleSheet("QLineEdit {color: red; background-color: black; selection-color: blue; selection-background-color: pink; font-family: sans-serif}");
    ui->pushButton->setStyleSheet("QPushButton {background-color: red; border-style: outset; border-width: 2px; border-radius: 10px; border-color: beige; font: bold 14px; min-width: 10em; padding: 6px;}");
    ui->pushButton_2->setStyleSheet("QPushButton {background-color: red; border-style: outset; border-width: 2px; border-radius: 10px; border-color: beige; font: bold 14px; min-width: 10em; padding: 6px;}");
    ui->pushButton_3->setStyleSheet("QPushButton {background-color: red; border-style: outset; border-width: 2px; border-radius: 10px; border-color: beige; font: bold 14px; min-width: 10em; padding: 6px;}");
    ui->pushButton_4->setStyleSheet("QPushButton {background-color: red; border-style: outset; border-width: 2px; border-radius: 10px; border-color: beige; font: bold 14px; min-width: 10em; padding: 6px;}");
    ui->textEdit->setFontPointSize(12);

    connect(ui->lineEdit, SIGNAL(textChanged(QString)), this, SLOT(checkInput(QString)));
    ui->plainTextEdit->setReadOnly(true);
    ui->plainTextEdit_7->setReadOnly(true);
    ui->plainTextEdit_8->setReadOnly(true);

    ui->gridLayout->menuBar();

    counter = 10;
    iterator = 0;
     questionform->add_question({"When was Jesus born?",{std::pair{"Year 1 BC",0},{"Year 0",1},{"Year 1 AD",0},{"Year 2 AD",0}}});
     questionform->add_question({"Who is the alter-ego of Superman?",{std::pair{"Bruce Wayne",0},{"Oliver Queen",0},{"Clarke Kent",1},{"Peter Parker",0}}});
     questionform->add_question({"Who was Kurt Gödel?",{std::pair{"A musician",0},{"A mathematician",1},{"A cartographer",0},{"A maniac",0}}});
     questionform->add_question({"What is the meaning of agapi in greece?",{std::pair{"Love",1},{"Hatred",0},{"Sad",0},{"Happy",0}}});
     questionform->add_question({"What is Gamecube?",{std::pair{"A sword",0},{"A monitor",0},{"A tablet",0},{"A video game console",1}}});
     questionform->add_question({"When was Jon Bon Jovi born?",{std::pair{"March 2, 1962",1},{"January 6, 1962",0},{"April 12, 1961",0},{"August 24, 1962",0}}});

    ui->textEdit_4->viewport()->setAutoFillBackground(false);
    ui->textEdit_4->setFrameShape(QFrame::NoFrame);
    ui->textEdit_5->viewport()->setAutoFillBackground(false);
    ui->textEdit_5->setFrameShape(QFrame::NoFrame);
    ui->textEdit_4->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->textEdit_4->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    animation_color = new QPropertyAnimation(ui->textEdit_4, "geometry");
    animation_color->setDuration(1000);

    animation_color->setStartValue(QRect(560,70,10,10));
    animation_color->setEndValue(ui->textEdit_4->geometry());
    ui->textEdit_4->setTextColor(Qt::red);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::paintEvent(QPaintEvent *)
{
    QRectF rectangle(70.0, -85.0, 40.0, 40.0);
    int startAngle = 30 * 16;
    int spanAngle = 360 * 16;
    QColor hourColor(127, 0, 127);

    int side = qMin(width(), height());


    QPainter painter(this);


    painter.setRenderHint(QPainter::Antialiasing);
    painter.translate(width() / 2, height() / 2);
    painter.scale(side / 200.0, side / 200.0);
    painter.setBrush(hourColor);
    painter.drawChord(rectangle, startAngle, spanAngle);
}

void MainWindow::update_time()
{
    counter--;
    qDebug() << QString("Time: %1 seconds").arg(counter);
    if(counter%10 == 0 || counter == 0)
    {
        iterator += 1;
        if(iterator != questionform->return_question().size()-1)
        {
            counter = 10;
            ui->plainTextEdit->setPlainText(questionform->return_question().at(iterator).first);
            ui->pushButton->setText(questionform->return_question().at(iterator).second.at(0).first);
            ui->pushButton_2->setText(questionform->return_question().at(iterator).second.at(1).first);
            ui->pushButton_3->setText(questionform->return_question().at(iterator).second.at(2).first);
            ui->pushButton_4->setText(questionform->return_question().at(iterator).second.at(3).first);
        }
        else
        {
            timer->stop();
            ui->stackedWidget->setCurrentIndex(2);
        }
    }
}
void MainWindow::update_color()
{

    animation_color->start();
    if(counter%2 == 0)
    {
        ui->textEdit_4->clear();
        ui->textEdit_4->setText(QString::number(counter-1));
        ui->textEdit_4->setTextColor(Qt::green);


    }
    if(counter%2 == 1)
    {
        ui->textEdit_4->clear();
        ui->textEdit_4->setText(QString::number(counter-1));
        ui->textEdit_4->setTextColor(Qt::red);
    }
}
void MainWindow::find_button()
{
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender()); // retrieve the button you have clicked
    QString buttonText = buttonSender->text(); // retrive the text from the button clicked
    qInfo() << buttonText;

}

void MainWindow::on_pushButton_25_clicked()
{
    QString text = ui->lineEdit->text();
    users.append(text);
    points = 0;
    iterator = 0;
    ui->plainTextEdit_7->insertPlainText(users.back());
    ui->plainTextEdit_8->insertPlainText(QString::number(0));
    ui->stackedWidget->setCurrentIndex(1);
    ui->plainTextEdit->setPlainText(questionform->return_question().at(iterator).first);
    ui->pushButton->setText(questionform->return_question().at(iterator).second.at(0).first);
    ui->pushButton_2->setText(questionform->return_question().at(iterator).second.at(1).first);
    ui->pushButton_3->setText(questionform->return_question().at(iterator).second.at(2).first);
    ui->pushButton_4->setText(questionform->return_question().at(iterator).second.at(3).first);
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()), this ,SLOT(update_time()));
    timer->start(1000);
    timer_of_color = new QTimer(this);
    connect(timer_of_color,SIGNAL(timeout()), this ,SLOT(update_color()));
    timer_of_color->start(1000);
}

void MainWindow::on_pushButton_clicked()
{
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender()); // retrieve the button you have clicked
    QString buttonText = buttonSender->text(); // retrive the text from the button clicked
    qInfo() << buttonText;
    counter = 10;
    //ui->stackedWidget->setCurrentIndex(2);
    if(questionform->find_answer(iterator) == buttonText)
    {
        points += 1;
    }
    ui->plainTextEdit_8->clear();
    ui->plainTextEdit_8->insertPlainText(QString::number(points));
    iterator += 1;
    qInfo() << buttonText;
    qInfo() << questionform->find_answer(iterator);
    qInfo() << points;
    if(iterator != questionform->return_question().size()-1)
    {
        ui->plainTextEdit->setPlainText(questionform->return_question().at(iterator).first);
        ui->pushButton->setText(questionform->return_question().at(iterator).second.at(0).first);
        ui->pushButton_2->setText(questionform->return_question().at(iterator).second.at(1).first);
        ui->pushButton_3->setText(questionform->return_question().at(iterator).second.at(2).first);
        ui->pushButton_4->setText(questionform->return_question().at(iterator).second.at(3).first);
    }
    else
    {
        timer->stop();
        ui->stackedWidget->setCurrentIndex(2);
    }

}


void MainWindow::on_pushButton_3_clicked()
{
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender()); // retrieve the button you have clicked
    QString buttonText = buttonSender->text(); // retrive the text from the button clicked
    qInfo() << buttonText;
    counter = 10;
    if(questionform->find_answer(iterator) == buttonText)
    {
        points += 1;
    }
    ui->plainTextEdit_8->clear();
    ui->plainTextEdit_8->insertPlainText(QString::number(points));
    iterator += 1;
    qInfo() << buttonText;
    qInfo() << questionform->find_answer(iterator);
    qInfo() << points;
    if(iterator != questionform->return_question().size()-1)
    {
        ui->plainTextEdit->setPlainText(questionform->return_question().at(iterator).first);
        ui->pushButton->setText(questionform->return_question().at(iterator).second.at(0).first);
        ui->pushButton_2->setText(questionform->return_question().at(iterator).second.at(1).first);
        ui->pushButton_3->setText(questionform->return_question().at(iterator).second.at(2).first);
        ui->pushButton_4->setText(questionform->return_question().at(iterator).second.at(3).first);
    }
    else
    {
        timer->stop();

        if(high_score.isEmpty())
        {
            high_score.append({users.back(),points});
        }
        else
        {
            for(int i = 0; i < high_score.size(); i++)
            {
                if(high_score.at(i).second < points)
                {
                    //high_score.append({users.back(),points});
                    high_score.insert(i,{users.back(),points});
                }
            }
        }
        ui->textEdit_5->clear();

        for(int i = 0; i < high_score.size(); i++)
        {
            //ui->textEdit_5->setText(high_score.at(i).first + " " + QString::number(high_score.at(i).second));
            qInfo() << high_score.at(i).first + " " + QString::number(high_score.at(i).second);
        }

        ui->stackedWidget->setCurrentIndex(2);
    }
}


void MainWindow::on_pushButton_2_clicked()
{
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender()); // retrieve the button you have clicked
    QString buttonText = buttonSender->text(); // retrive the text from the button clicked
    qInfo() << buttonText;
    counter = 10;
    if(questionform->find_answer(iterator) == buttonText)
    {
        points += 1;
    }
    ui->plainTextEdit_8->clear();
    ui->plainTextEdit_8->insertPlainText(QString::number(points));
    iterator += 1;
    qInfo() << buttonText;
    qInfo() << questionform->find_answer(iterator);
    qInfo() << points;
    if(iterator != questionform->return_question().size()-1)
    {
        ui->plainTextEdit->setPlainText(questionform->return_question().at(iterator).first);
        ui->pushButton->setText(questionform->return_question().at(iterator).second.at(0).first);
        ui->pushButton_2->setText(questionform->return_question().at(iterator).second.at(1).first);
        ui->pushButton_3->setText(questionform->return_question().at(iterator).second.at(2).first);
        ui->pushButton_4->setText(questionform->return_question().at(iterator).second.at(3).first);
    }
    else
    {
        timer->stop();
        ui->stackedWidget->setCurrentIndex(2);
    }
}


void MainWindow::on_pushButton_4_clicked()
{
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender()); // retrieve the button you have clicked
    QString buttonText = buttonSender->text(); // retrive the text from the button clicked
    qInfo() << buttonText;
    counter = 10;
    if(questionform->find_answer(iterator) == buttonText)
    {
        points += 1;
    }
    ui->plainTextEdit_8->clear();
    ui->plainTextEdit_8->insertPlainText(QString::number(points));
    iterator += 1;
    qInfo() << buttonText;
    qInfo() << questionform->find_answer(iterator);
    qInfo() << points;
    if(iterator != questionform->return_question().size()-1)
    {
        ui->plainTextEdit->setPlainText(questionform->return_question().at(iterator).first);
        ui->pushButton->setText(questionform->return_question().at(iterator).second.at(0).first);
        ui->pushButton_2->setText(questionform->return_question().at(iterator).second.at(1).first);
        ui->pushButton_3->setText(questionform->return_question().at(iterator).second.at(2).first);
        ui->pushButton_4->setText(questionform->return_question().at(iterator).second.at(3).first);
    }
    else
    {
        timer->stop();
        ui->stackedWidget->setCurrentIndex(2);
    }
}

void MainWindow::checkInput(const QString &text)
{
    ui->pushButton_25->setEnabled(!text.isEmpty());
}


void MainWindow::on_pushButton_27_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
}


void MainWindow::on_pushButton_29_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}


void MainWindow::on_pushButton_28_clicked()
{
    QString text = ui->textEdit_3->toPlainText();
    QString text2 = ui->textEdit_9->toPlainText();
    QString text3 = ui->textEdit_17->toPlainText();
    QString text4 = ui->textEdit_15->toPlainText();
    QString text5 = ui->textEdit_16->toPlainText();
    QString text6 = ui->radioButton->text();
    QString text7 = ui->radioButton_2->text();
    QString text8 = ui->radioButton_3->text();
    QString text9 = ui->radioButton_4->text();

    if(text6 == QString::fromStdString("1"))
    {
        questionform->add_question({text,{std::pair{text2,1},{text3,0},{text4,0},{text5,0}}});
        ui->radioButton->setAutoExclusive(false);
        ui->radioButton->setChecked(false);
        ui->radioButton->setAutoExclusive(true);
    }
    if(text7 == QString::fromStdString("2"))
    {
        questionform->add_question({text,{std::pair{text2,0},{text3,1},{text4,0},{text5,0}}});
        ui->radioButton_2->setAutoExclusive(false);
        ui->radioButton_2->setChecked(false);
        ui->radioButton_2->setAutoExclusive(true);
    }
    if(text7 == QString::fromStdString("3"))
    {
        questionform->add_question({text,{std::pair{text2,0},{text3,0},{text4,1},{text5,0}}});
        ui->radioButton_3->setAutoExclusive(false);
        ui->radioButton_3->setChecked(false);
        ui->radioButton_3->setAutoExclusive(true);
    }
    if(text7 == QString::fromStdString("4"))
    {
        questionform->add_question({text,{std::pair{text2,0},{text3,0},{text4,0},{text5,1}}});
        ui->radioButton_4->setAutoExclusive(false);
        ui->radioButton_4->setChecked(false);
        ui->radioButton_4->setAutoExclusive(true);
    }
    qInfo() << text;
    qInfo() << text2;
    qInfo() << text3;
    qInfo() << text4;
    qInfo() << text5;
    qInfo() << text6;
    qInfo() << text7;
    qInfo() << text8;
    qInfo() << text9;

    ui->textEdit_3->clear();
    ui->textEdit_9->clear();
    ui->textEdit_17->clear();
    ui->textEdit_15->clear();
    ui->textEdit_16->clear();
}

void MainWindow::on_pushButton_5_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}


void MainWindow::on_pushButton_26_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

