#ifndef SMOOTHARC_H
#define SMOOTHARC_H

#include<QtWidgets>
#include <QWidget>
#include <QPropertyAnimation>
#include <QRandomGenerator>
class SmoothArc : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(qreal value READ value WRITE setValue NOTIFY valueChanged)
public:
    SmoothArc();

    qreal value() const;
    void setValue(qreal value);

signals:
    void valueChanged();

protected:
    void timerEvent(QTimerEvent *) Q_DECL_OVERRIDE;
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;

private:
    int m_valueTimerId;
    qreal m_value;
    QPropertyAnimation m_animation;
};

#endif // SMOOTHARC_H
