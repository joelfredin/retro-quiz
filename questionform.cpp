#include "questionform.h"

QuestionForm::QuestionForm()
{

}

void QuestionForm::add_question(std::pair<QString,std::array<std::pair<QString, int>,4>> question)
{
    questions.append(question);
}

QVector<std::pair<QString,std::array<std::pair<QString, int>,4>>> QuestionForm::return_question()
{
    return questions;
}

int QuestionForm::number_of_questions()
{
    return questions.size();
}

QString QuestionForm::find_answer(qint32 i)
{
    for(std::pair<QString, int> answer : questions.at(i).second)
    {
        if(answer.second == 1)
        {
            return answer.first;
        }
    }
    return "";
}
