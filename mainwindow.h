#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
//#include <QTimer>
#include "newcircle.h"
#include<QPropertyAnimation>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
//    QTimer *timer;
    void fullscreenwidget();

private:
    qint32  counter;
    qint32  iterator;
    QTimer * timer;
    QTimer * new_timer;
    //MyCircle circle1;
    NewCircle *circle1;
    QPropertyAnimation *animation;
    QPropertyAnimation *animation_color;
    QTimer * timer_of_color;

public slots:
    //virtual void timerDone();
private slots:
    void on_pushButton_clicked();
    void paintEvent(QPaintEvent *);
    void find_button();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_25_clicked();

    void checkInput(const QString &text);

    void update_time();

    void on_pushButton_27_clicked();

    void on_pushButton_29_clicked();

    void on_pushButton_28_clicked();

    void update_color();

    void on_pushButton_5_clicked();

    void on_pushButton_26_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
