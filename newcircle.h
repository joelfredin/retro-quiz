#ifndef NEWCIRCLE_H
#define NEWCIRCLE_H
#include<QGraphicsItem>
#include<QPainter>
#include "qpen.h"

class NewCircle : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_PROPERTY(QPointF pos READ pos WRITE setPos)
public:
    NewCircle();
    ~NewCircle();
    //~NewCircle();
    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

};

#endif // NEWCIRCLE_H
