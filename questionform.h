#ifndef QUESTIONFORM_H
#define QUESTIONFORM_H
#include<QVector>

class QuestionForm
{
public:
    QuestionForm();
    void add_question(std::pair<QString,std::array<std::pair<QString, int>,4>> question);
    QVector<std::pair<QString,std::array<std::pair<QString, int>,4>>> return_question();
    int number_of_questions();
    QString find_answer(qint32 i);
private:
    QVector<std::pair<QString,std::array<std::pair<QString, int>,4>>> questions;
};

#endif // QUESTIONFORM_H
