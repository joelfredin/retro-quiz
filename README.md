# Introduction

This is a quiz-program, in the style of Kahoot. It was made using QtWidgets.

# How to Run the Program?
You simply download the files, and use qt to run the application.

# How to Play?
When you run the application, you will meet a starting screen with three buttons. One called "Play", one called "High Score" and one called "Add Question". To play the actual game, you press "Play". To watch the highest scores, press the "High Score" button.

Once you press "Play" You will get a question, and 4 different alternatives. You have 10 seconds to answer the question, and the faster you are, the higher score you get.

If you press "Add Question" at the startscreen, you can add your own questions to the game together with four alternatives. You will then also play these questions the next time you play the game!

Have fun.
